function createCard(name, description, pictureUrl,starts,ends,locationName) {
    return `
        <div class="col">
            <div class="card shadow p-3 mb-5 bg-white rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-subtitle text-muted">${locationName}</p>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">${starts} - ${ends}</div>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    // throw new Error("test")
    const response = await fetch(url);

    if (!response.ok) {
        return response.json();
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          const startDate = new Date(details.conference.starts);
          const starts = startDate.toLocaleDateString();
          const endDate = new Date(details.conference.ends);
          const ends = endDate.toLocaleDateString();

          const locationName = details.conference.location.name;

          const html = createCard(title, description, pictureUrl,starts,ends,locationName);
          const column = document.querySelector('.row.row-cols-3');
          column.innerHTML += html;


        }
      }

    }
  } catch (e) {
    const errors = document.querySelector(".error")
    const html = '<div class="alert alert-danger" role="alert">ERROR</div>'
    errors.innerHTML += html;
  }

});

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//             return response.json();
//         } else {
//             const data = await response.json();
//             // console.log(data)
//             const conference = data.conferences[0];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;

//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 // console.log(details);

//                 const description = details.conference.description;
//                 const descTag = document.querySelector('.card-text');
//                 descTag.innerHTML = description;

//                 const imageTag = document.querySelector('.card-img-top');
//                 imageTag.src = details.conference.location.picture_url;

//             }

//         }
//     }   catch (e) {
//         console.error('error', e);
//     }
// });
